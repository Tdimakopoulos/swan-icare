/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generator;

import java.util.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hl7.ws.assoc.Hl7Associations;
import org.hl7.ws.doctor.Hl7Doctor;
import org.hl7.ws.meddev.Hl7MedicalDevices;
import org.hl7.ws.obs.Hl7Observation;
import org.hl7.ws.patient.Hl7Patient;
import org.hl7.ws.security.UserSecurity_Type;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class MakeInitialRecords {

    // Doctor            = 1
    // Patient 1         = 2
    // Patient 2         = 3
    // Medical 1         = 4
    // Medical 2         = 5
    // User
    
    public static Random numGen =new Random();
    
    public int RandNum(){
    int rand = Math.abs((100)+numGen.nextInt(100));

    return rand;
}

    
    public static void main(String[] args) throws InterruptedException {
        MakeInitialRecords pp = new MakeInitialRecords();
//    pp.CreateDoctor();
//pp.CreatePat();
//pp.CreateMed();
//org.hl7.ws.assoc.Hl7Associations hL7Associations = new Hl7Associations();
//hL7Associations.setDoctorid(new Long(1));
//hL7Associations.setPatientid(new Long(3));
//hL7Associations.setMedicalid(new Long(5));
//createAssociations(hL7Associations);
//org.hl7.ws.security.UserSecurity_Type userSecurity = new UserSecurity_Type();
//userSecurity.setIperson(new Long(1));
//userSecurity.setPassword("doctor");
//userSecurity.setUsername("doctor");
//userSecurity.setIrole(new Long(10));
//createUser(userSecurity);
// 1 2 3 5 6 9 10 11 15 23 
pp.Make("6");
pp.Make("9");
pp.Make("10");
pp.Make("11");
pp.Make("15");
pp.Make("23");
        
    }
    
    public void Make(String dd)
    {
        for (int i = 0; i < 20; i++) {
            try {
                org.hl7.ws.obs.Hl7Observation hL7Observatio= new Hl7Observation();
                hL7Observatio.setLpatient(new Long(2));
                hL7Observatio.setName(dd);
                hL7Observatio.setLngdate((new Date()).getTime());
                hL7Observatio.setIntvalue(RandNum());
                create(hL7Observatio);
                Thread.sleep(5000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MakeInitialRecords.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    }
    public void CreatePat() {
        org.hl7.ws.patient.Hl7Patient hL7Patient = new Hl7Patient();
        hL7Patient.setName("Helen");
        hL7Patient.setSurname("Walker");
        hL7Patient.setEmail("Hwalker@hotmail.com");
        createPat(hL7Patient);
        System.out.println(findAllPats().get(1).getId());
    }

    public void CreateDoctor() {
        org.hl7.ws.doctor.Hl7Doctor hL7Doctor = new Hl7Doctor();
        hL7Doctor.setName("Marcus");
        hL7Doctor.setSurname("Andrew");
        hL7Doctor.setDepartment("General Pathology");
        hL7Doctor.setEmail("marcus@hl7hospital.com");
        createDoctor(hL7Doctor);
        System.out.println(findAllDoctors().get(0).getId());
    }

    private static java.util.List<org.hl7.ws.doctor.Hl7Doctor> findAllDoctors() {
        org.hl7.ws.doctor.DoctorHL7_Service service = new org.hl7.ws.doctor.DoctorHL7_Service();
        org.hl7.ws.doctor.DoctorHL7 port = service.getDoctorHL7Port();
        return port.findAll();
    }

    private static void createDoctor(org.hl7.ws.doctor.Hl7Doctor hL7Doctor) {
        org.hl7.ws.doctor.DoctorHL7_Service service = new org.hl7.ws.doctor.DoctorHL7_Service();
        org.hl7.ws.doctor.DoctorHL7 port = service.getDoctorHL7Port();
        port.create(hL7Doctor);
    }

    private static void createPat(org.hl7.ws.patient.Hl7Patient hL7Patient) {
        org.hl7.ws.patient.PatientHL7_Service service = new org.hl7.ws.patient.PatientHL7_Service();
        org.hl7.ws.patient.PatientHL7 port = service.getPatientHL7Port();
        port.create(hL7Patient);
    }

    private static java.util.List<org.hl7.ws.patient.Hl7Patient> findAllPats() {
        org.hl7.ws.patient.PatientHL7_Service service = new org.hl7.ws.patient.PatientHL7_Service();
        org.hl7.ws.patient.PatientHL7 port = service.getPatientHL7Port();
        return port.findAll();
    }

    private static void createUser(org.hl7.ws.security.UserSecurity_Type userSecurity) {
        org.hl7.ws.security.UserSecurity_Service service = new org.hl7.ws.security.UserSecurity_Service();
        org.hl7.ws.security.UserSecurity port = service.getUserSecurityPort();
        port.create(userSecurity);
    }

    private static java.util.List<org.hl7.ws.security.UserSecurity_Type> findAllUser() {
        org.hl7.ws.security.UserSecurity_Service service = new org.hl7.ws.security.UserSecurity_Service();
        org.hl7.ws.security.UserSecurity port = service.getUserSecurityPort();
        return port.findAll();
    }

    public void CreateMed() {
        org.hl7.ws.meddev.Hl7MedicalDevices hL7MedicalDevices = new Hl7MedicalDevices();
        hL7MedicalDevices.setName("Device 2");
        hL7MedicalDevices.setSerial("2120-44293-20293-2");
        createMed(hL7MedicalDevices);
        System.out.println(findAllMed().get(1).getId());
    }

    private static void createMed(org.hl7.ws.meddev.Hl7MedicalDevices hL7MedicalDevices) {
        org.hl7.ws.meddev.MedicaldeviceHL7_Service service = new org.hl7.ws.meddev.MedicaldeviceHL7_Service();
        org.hl7.ws.meddev.MedicaldeviceHL7 port = service.getMedicaldeviceHL7Port();
        port.create(hL7MedicalDevices);
    }

    private static java.util.List<org.hl7.ws.meddev.Hl7MedicalDevices> findAllMed() {
        org.hl7.ws.meddev.MedicaldeviceHL7_Service service = new org.hl7.ws.meddev.MedicaldeviceHL7_Service();
        org.hl7.ws.meddev.MedicaldeviceHL7 port = service.getMedicaldeviceHL7Port();
        return port.findAll();
    }

    private static void createAssociations(org.hl7.ws.assoc.Hl7Associations hL7Associations) {
        org.hl7.ws.assoc.AssociationHL7_Service service = new org.hl7.ws.assoc.AssociationHL7_Service();
        org.hl7.ws.assoc.AssociationHL7 port = service.getAssociationHL7Port();
        port.create(hL7Associations);
    }

    private static void create(org.hl7.ws.obs.Hl7Observation hL7Observation) {
        org.hl7.ws.obs.Obshl7_Service service = new org.hl7.ws.obs.Obshl7_Service();
        org.hl7.ws.obs.Obshl7 port = service.getObshl7Port();
        port.create(hL7Observation);
    }

}
