/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.dashboard;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.event.AjaxBehaviorEvent;
import javax.xml.ws.WebServiceRef;
import org.hl7.model.medicalinfo;
import org.hl7.model.patientinfo;
import org.hl7.model.patientlistbox;
import org.hl7.ws.assoc.AssociationHL7_Service;
import org.hl7.ws.doctor.DoctorHL7_Service;
import org.hl7.ws.doctor.Hl7Doctor;
import org.hl7.ws.meddev.MedicaldeviceHL7_Service;
import org.hl7.ws.obs.Obshl7_Service;
import org.hl7.ws.patient.PatientHL7_Service;
import org.hl7.ws.security.UserSecurity_Service;
import org.primefaces.model.chart.AxisType;
import org.primefaces.model.chart.DateAxis;
import org.primefaces.model.chart.LineChartModel;
import org.primefaces.model.chart.LineChartSeries;

/**
 *
 * @author tdim
 */
@ManagedBean
@SessionScoped
public class dashboard {

    @WebServiceRef(wsdlLocation = "WEB-INF/wsdl/localhost_8080/medicaldeviceHL7/medicaldeviceHL7.wsdl")
    private MedicaldeviceHL7_Service service_5;

    private Obshl7_Service service_4;

    private AssociationHL7_Service service_3;
    private PatientHL7_Service service_2;
    java.util.List<org.hl7.ws.assoc.Hl7Associations> pConn;
    private List<patientinfo> patientitems;
    private List<medicalinfo> medicalitems;
    private DoctorHL7_Service service_1;
    private UserSecurity_Service service;
    private Long DoctorID;
    private List<String> items;
    private String szUsername;
    private String szPassword;
    private String doctorName;
    private List<patientlistbox> plistbox;
    private patientlistbox Selectedpatient;
    private String selpat;
    private String[] selectedmes;
    private List<String> mes;
    private LineChartModel dateModel;
    private LineChartModel dateModel2;
    private LineChartModel dateModel3;
    private LineChartModel dateModel5;
    private LineChartModel dateModel6;
    private LineChartModel dateModel9;
    private LineChartModel dateModel10;
    private LineChartModel dateModel11;
    private LineChartModel dateModel15;
    private LineChartModel dateModel23;

    private boolean b1;
    private boolean b2;
    private boolean b3;
    private boolean b5;
    private boolean b6;
    private boolean b9;
    private boolean b10;
    private boolean b11;
    private boolean b15;
    private boolean b23;
    
    public void AddMess() {
        mes = new ArrayList<String>();
        mes.add(Convert(1));
        mes.add(Convert(2));
        mes.add(Convert(3));
        mes.add(Convert(5));
        mes.add(Convert(6));
        mes.add(Convert(9));
        mes.add(Convert(10));
        mes.add(Convert(11));
        mes.add(Convert(15));
        mes.add(Convert(23));
    }

    public List<String> getItems() {
        items = new ArrayList();
        items.add("shirt");
        items.add("skirt");
        items.add("trouser");
        return items;
    }

    /**
     * Creates a new instance of dashboard
     */
    public dashboard() {
        patientitems = new ArrayList();
        medicalitems = new ArrayList();
        plistbox = new ArrayList();
    }

    public void LoadConnections() {
        pConn = findAll_2();
    }

    public boolean CheckifPAndD(Long DID, Long PID) {
        for (int i = 0; i < pConn.size(); i++) {
            if (pConn.get(i).getDoctorid().equals(DID)) {
                if (pConn.get(i).getPatientid().equals(PID)) {
                    return true;
                }
            }
        }
        return false;
    }

    public String Convert(int itype) {

        if (itype == 1) {
            return "MMP-9_DARK";
        }
        if (itype == 2) {
            return "MMP-9_630";
        }
        if (itype == 3) {
            return "MMP-9_850";
        }
        if (itype == 4) {
            return "MMPSENS_STATUS";
        }
        if (itype == 5) {
            return "pH1";
        }
        if (itype == 6) {
            return "pH2";
        }
        if (itype == 7) {
            return "pH1SNES_STATUS";
        }
        if (itype == 8) {
            return "pH2SENS_STATUS";
        }
        if (itype == 9) {
            return "T1";
        }
        if (itype == 10) {
            return "T2";
        }
        if (itype == 11) {
            return "T3";
        }
        if (itype == 12) {
            return "T1SENS_STATUS";
        }
        if (itype == 13) {
            return "T2SENS_STATUS";
        }
        if (itype == 14) {
            return "T3SENS_STATUS";
        }
        if (itype == 15) {
            return "CBP";
        }
        if (itype == 16) {
            return "CBP_STATUS";
        }
        if (itype == 17) {
            return "BATT_SOC";
        }
        if (itype == 18) {
            return "BATT_CAP";
        }
        if (itype == 19) {
            return "BATT_V";
        }
        if (itype == 20) {
            return "BATT_I";
        }
        if (itype == 21) {
            return "BATT_TEMP";
        }
        if (itype == 22) {
            return "BATT_STATUS";
        }
        if (itype == 23) {
            return "IWSD_ATTITUDE";
        }
        if (itype == 24) {
            return "IWSD_STATUS";
        }
        return "UNKNOWN";
    }

    public void LoadPatients() {
        LoadConnections();
        AddMess();
        java.util.List<org.hl7.ws.patient.Hl7Patient> ppat = findAll_1();
        patientitems.clear();
        getMedicalitems().clear();
        java.util.List<org.hl7.ws.meddev.Hl7MedicalDevices> pmedical = findAll_4();
        for(int d=0;d<pmedical.size();d++)
        {
            medicalinfo pmedinf=new medicalinfo();
            pmedinf.setSerial(pmedical.get(d).getSerial());
            pmedinf.setName(pmedical.get(d).getName());
            pmedinf.setStatus("Working!");
            getMedicalitems().add(pmedinf);
        }
        
        for (int i = 0; i < ppat.size(); i++) {

            if (CheckifPAndD(DoctorID, ppat.get(i).getId())) {
                patientinfo pinfo = new patientinfo();
                pinfo.setName(ppat.get(i).getName() + " " + ppat.get(i).getSurname());
                pinfo.setCurrentstatus("Offline");
                pinfo.setDevicestatus("Offline");
                pinfo.setMedicaldevid("");
                pinfo.setMedicaldevstatus("Offline");
                pinfo.setStatus("Active");
                patientitems.add(pinfo);

                patientlistbox pl = new patientlistbox();
                pl.setId(ppat.get(i).getId().toString());
                pl.setName(ppat.get(i).getName() + " " + ppat.get(i).getSurname());
                plistbox.add(pl);

            }
        }
    }

    public void Refresh() {
        LoadPatients();

    }

    private boolean CheckDetails() {
        java.util.List<org.hl7.ws.security.UserSecurity_Type> pusers = findAll();

        for (int i = 0; i < pusers.size(); i++) {
            if (pusers.get(i).getUsername().equalsIgnoreCase(szUsername)) {
                if (pusers.get(i).getPassword().equalsIgnoreCase(szPassword)) {
                    if (pusers.get(i).getIrole().equals(new Long(10))) {
                        Hl7Doctor pd = find(pusers.get(i).getIperson());
                        doctorName = pd.getName() + "  " + pd.getSurname();
//                        System.out.println("From DB : "+pusers.get(i).getIperson());
                        DoctorID = pusers.get(i).getIperson();
                        Refresh();
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public String convertDate(Long mil) {
        DateFormat df = new SimpleDateFormat("HH:mm:ss");

        // Get the date today using Calendar object.
        Date today = new Date(mil);
        // Using DateFormat format method we can create a string 
        // representation of a date with the defined format.
        String reportDate = df.format(today);
        return reportDate;
    }


    public void SelectGraphs()
    {
        // 1 2 3 5 6 9 10 11 15 23
//        selectedmes
//         "MMP-9_DARK";
//         "MMP-9_630";
//        "MMP-9_850";
//        "pH1";
//        "pH2";
//        "T1";
//        "T2";
//        "T3";
//        "CBP";
//        "IWSD_ATTITUDE";
//
b1=false;
    b2=false;
    b3=false;
    b5=false;
    b6=false;
    b9=false;
    b10=false;
    b11=false;
    b15=false;
    b23=false;

        for(int i=0;i<selectedmes.length;i++)
        {
            if(selectedmes[i].equalsIgnoreCase("MMP-9_DARK"))
                setB1(true);
            
            if(selectedmes[i].equalsIgnoreCase("MMP-9_630"))
                setB2(true);
            
            
            if(selectedmes[i].equalsIgnoreCase("MMP-9_850"))
                setB3(true);
            
            
            if(selectedmes[i].equalsIgnoreCase("pH1"))
                setB5(true);
            
            
            if(selectedmes[i].equalsIgnoreCase("pH2"))
                setB6(true);
            
            
            if(selectedmes[i].equalsIgnoreCase("T1"))
                setB9(true);
            
            if(selectedmes[i].equalsIgnoreCase("T2"))
                setB10(true);
            
            
            if(selectedmes[i].equalsIgnoreCase("T3"))
                setB11(true);
            
            
            if(selectedmes[i].equalsIgnoreCase("CBP"))
                setB15(true);
            
            if(selectedmes[i].equalsIgnoreCase("IWSD_ATTITUDE"))
                setB23(true);
        }
    }
    public void LoadGraphData() {
SelectGraphs();
        dateModel = new LineChartModel();
        dateModel2 = new LineChartModel();
        dateModel3 = new LineChartModel();
        dateModel5 = new LineChartModel();
        dateModel6 = new LineChartModel();
        dateModel9 = new LineChartModel();
        dateModel10 = new LineChartModel();
        dateModel11 = new LineChartModel();
        dateModel15 = new LineChartModel();
        dateModel23 = new LineChartModel();

        LineChartSeries series2 = new LineChartSeries();
        series2.setLabel("Series 2");
        String lastdate2 = null;

        LineChartSeries series3 = new LineChartSeries();
        series3.setLabel("Series 3");
        String lastdate3 = null;

        LineChartSeries series5 = new LineChartSeries();
        series5.setLabel("Series 5");
        String lastdate5 = null;

        LineChartSeries series6 = new LineChartSeries();
        series6.setLabel("Series 6");
        String lastdate6 = null;

        LineChartSeries series9 = new LineChartSeries();
        series9.setLabel("Series 9");
        String lastdate9 = null;

        LineChartSeries series10 = new LineChartSeries();
        series10.setLabel("Series 10");
        String lastdate10 = null;

        LineChartSeries series11 = new LineChartSeries();
        series11.setLabel("Series 11");
        String lastdate11 = null;

        LineChartSeries series15 = new LineChartSeries();
        series15.setLabel("Series 15");
        String lastdate15 = null;

        LineChartSeries series23 = new LineChartSeries();
        series23.setLabel("Series 23");
        String lastdate23 = null;

        LineChartSeries series1 = new LineChartSeries();
        series1.setLabel("Series 1");
        String lastdate = null;

        java.util.List<org.hl7.ws.obs.Hl7Observation> pobs = findAll_3();
        for (int i = 0; i < pobs.size(); i++) {
            //value of type of mes
//            pobs.get(i).getName()
            //patient id
//            pobs.get(i).getLpatient()
            //int value
//            pobs.get(i).getIntvalue();
            //date
//            pobs.get(i).getLngdate()

            if (pobs.get(i).getName().equalsIgnoreCase("1")) {
                series1.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("2")) {
                series2.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate2 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("3")) {
                series3.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate3 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("5")) {
                series5.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate5 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("6")) {
                series6.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate6 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("9")) {
                series9.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate9 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("10")) {
                series10.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate10 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("11")) {
                series11.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate11 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("15")) {
                series15.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate15 = convertDate(pobs.get(i).getLngdate());

            }

            if (pobs.get(i).getName().equalsIgnoreCase("23")) {
                series23.set(convertDate(pobs.get(i).getLngdate()), pobs.get(i).getIntvalue());
                lastdate23 = convertDate(pobs.get(i).getLngdate());

            }
        }
//        dateModel = new LineChartModel();
//        LineChartSeries series1 = new LineChartSeries();
//        series1.setLabel("Series 1");

//        series1.set("2014-01-01 00:10:50", 51);
//        series1.set("2014-01-01 00:10:51", 22);
//        series1.set("2014-01-01 00:10:52", 65);
//        series1.set("2014-01-01 00:10:53", 74);
//        series1.set("2014-01-01 00:10:55", 24);
//        series1.set("2014-01-01 00:10:56", 51);
        dateModel.addSeries(series1);
        dateModel.getAxis(AxisType.Y).setLabel(Convert(1));
        DateAxis axis = new DateAxis("Dates");
        axis.setTickAngle(-50);
        axis.setMax(lastdate);
        axis.setTickFormat("%H:%#M:%S");
        dateModel.getAxes().put(AxisType.X, axis);

        dateModel2.addSeries(series2);
        dateModel2.getAxis(AxisType.Y).setLabel(Convert(2));
        DateAxis axis2 = new DateAxis("Dates");
        axis2.setTickAngle(-50);
        axis2.setMax(lastdate2);
        axis2.setTickFormat("%H:%#M:%S");
        dateModel2.getAxes().put(AxisType.X, axis2);

        dateModel3.addSeries(series3);
        dateModel3.getAxis(AxisType.Y).setLabel(Convert(3));
        DateAxis axis3 = new DateAxis("Dates");
        axis3.setTickAngle(-50);
        axis3.setMax(lastdate3);
        axis3.setTickFormat("%H:%#M:%S");
        dateModel3.getAxes().put(AxisType.X, axis3);

        dateModel5.addSeries(series5);
        dateModel5.getAxis(AxisType.Y).setLabel(Convert(5));
        DateAxis axis5 = new DateAxis("Dates");
        axis5.setTickAngle(-50);
        axis5.setMax(lastdate5);
        axis5.setTickFormat("%H:%#M:%S");
        dateModel5.getAxes().put(AxisType.X, axis5);

        dateModel6.addSeries(series6);
        dateModel6.getAxis(AxisType.Y).setLabel(Convert(6));
        DateAxis axis6 = new DateAxis("Dates");
        axis6.setTickAngle(-50);
        axis6.setMax(lastdate6);
        axis6.setTickFormat("%H:%#M:%S");
        dateModel6.getAxes().put(AxisType.X, axis6);

        dateModel9.addSeries(series9);
        dateModel9.getAxis(AxisType.Y).setLabel(Convert(9));
        DateAxis axis9 = new DateAxis("Dates");
        axis9.setTickAngle(-50);
        axis9.setMax(lastdate9);
        axis9.setTickFormat("%H:%#M:%S");
        dateModel9.getAxes().put(AxisType.X, axis9);

        dateModel10.addSeries(series10);
        dateModel10.getAxis(AxisType.Y).setLabel(Convert(10));
        DateAxis axis10 = new DateAxis("Dates");
        axis10.setTickAngle(-50);
        axis10.setMax(lastdate10);
        axis10.setTickFormat("%H:%#M:%S");
        dateModel10.getAxes().put(AxisType.X, axis10);

        dateModel11.addSeries(series11);
        dateModel11.getAxis(AxisType.Y).setLabel(Convert(11));
        DateAxis axis11 = new DateAxis("Dates");
        axis11.setTickAngle(-50);
        axis11.setMax(lastdate11);
        axis11.setTickFormat("%H:%#M:%S");
        dateModel11.getAxes().put(AxisType.X, axis11);

        dateModel15.addSeries(series15);
        dateModel15.getAxis(AxisType.Y).setLabel(Convert(15));
        DateAxis axis15 = new DateAxis("Dates");
        axis15.setTickAngle(-50);
        axis15.setMax(lastdate15);
        axis15.setTickFormat("%H:%#M:%S");
        dateModel15.getAxes().put(AxisType.X, axis15);

        dateModel23.addSeries(series23);
        dateModel23.getAxis(AxisType.Y).setLabel(Convert(23));
        DateAxis axis23 = new DateAxis("Dates");
        axis23.setTickAngle(-50);
        axis23.setMax(lastdate23);
        axis23.setTickFormat("%H:%#M:%S");
        dateModel23.getAxes().put(AxisType.X, axis23);
    }

    public String selectpat() {
        System.out.println("-->>" + selpat);
        System.out.println("-->" + selectedmes);
        LoadGraphData();
        return "Observations_g?faces-redirect=true";
    }

    public String loginprocess() {

        System.out.println("-->");
        System.out.println(szUsername + " -- " + szPassword);
        if (CheckDetails()) {
            return "welcomePrimefaces?faces-redirect=true";
        } else {
            return "index?faces-redirect=true";
        }
    }

    /**
     * @return the szUsername
     */
    public String getSzUsername() {
        return szUsername;
    }

    /**
     * @param szUsername the szUsername to set
     */
    public void setSzUsername(String szUsername) {
        this.szUsername = szUsername;
    }

    /**
     * @return the szPassword
     */
    public String getSzPassword() {
        return szPassword;
    }

    /**
     * @param szPassword the szPassword to set
     */
    public void setSzPassword(String szPassword) {
        this.szPassword = szPassword;
    }

    /**
     * @return the doctorName
     */
    public String getDoctorName() {
        return doctorName;
    }

    /**
     * @param doctorName the doctorName to set
     */
    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    private java.util.List<org.hl7.ws.patient.Hl7Patient> findAll_1() {
        PatientHL7_Service service_2 = new PatientHL7_Service();
        org.hl7.ws.patient.PatientHL7 port = service_2.getPatientHL7Port();
        return port.findAll();
    }

    private java.util.List<org.hl7.ws.security.UserSecurity_Type> findAll() {
        UserSecurity_Service service = new UserSecurity_Service();
        org.hl7.ws.security.UserSecurity port = service.getUserSecurityPort();
        return port.findAll();
    }

    private Hl7Doctor find(java.lang.Object id) {
        DoctorHL7_Service service_1 = new DoctorHL7_Service();
        org.hl7.ws.doctor.DoctorHL7 port = service_1.getDoctorHL7Port();
        return port.find(id);
    }

    /**
     * @return the DoctorID
     */
    public Long getDoctorID() {
        return DoctorID;
    }

    /**
     * @param DoctorID the DoctorID to set
     */
    public void setDoctorID(Long DoctorID) {
        this.DoctorID = DoctorID;
    }

    /**
     * @return the patientitems
     */
    public List<patientinfo> getPatientitems() {
        return patientitems;
    }

    /**
     * @param patientitems the patientitems to set
     */
    public void setPatientitems(List<patientinfo> patientitems) {
        this.patientitems = patientitems;
    }

    private java.util.List<org.hl7.ws.assoc.Hl7Associations> findAll_2() {
        AssociationHL7_Service service_3 = new AssociationHL7_Service();
        org.hl7.ws.assoc.AssociationHL7 port = service_3.getAssociationHL7Port();
        return port.findAll();
    }

    /**
     * @return the plistbox
     */
    public List<patientlistbox> getPlistbox() {
        return plistbox;
    }

    /**
     * @param plistbox the plistbox to set
     */
    public void setPlistbox(List<patientlistbox> plistbox) {
        this.plistbox = plistbox;
    }

    /**
     * @return the Selectedpatient
     */
    public patientlistbox getSelectedpatient() {
        return Selectedpatient;
    }

    /**
     * @param Selectedpatient the Selectedpatient to set
     */
    public void setSelectedpatient(patientlistbox Selectedpatient) {
        this.Selectedpatient = Selectedpatient;
    }

    /**
     * @return the selpat
     */
    public String getSelpat() {
        return selpat;
    }

    /**
     * @param selpat the selpat to set
     */
    public void setSelpat(String selpat) {
        this.selpat = selpat;
    }

    /**
     * @return the selectedmes
     */
    public String[] getSelectedmes() {
        return selectedmes;
    }

    /**
     * @param selectedmes the selectedmes to set
     */
    public void setSelectedmes(String[] selectedmes) {
        this.selectedmes = selectedmes;
    }

    /**
     * @return the mes
     */
    public List<String> getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(List<String> mes) {
        this.mes = mes;
    }

    /**
     * @return the dateModel
     */
    public LineChartModel getDateModel() {
        return dateModel;
    }

    /**
     * @param dateModel the dateModel to set
     */
    public void setDateModel(LineChartModel dateModel) {
        this.dateModel = dateModel;
    }

    private java.util.List<org.hl7.ws.obs.Hl7Observation> findAll_3() {
        Obshl7_Service service_4 = new Obshl7_Service();
        org.hl7.ws.obs.Obshl7 port = service_4.getObshl7Port();
        return port.findAll();
    }

    /**
     * @return the dateModel2
     */
    public LineChartModel getDateModel2() {
        return dateModel2;
    }

    /**
     * @param dateModel2 the dateModel2 to set
     */
    public void setDateModel2(LineChartModel dateModel2) {
        this.dateModel2 = dateModel2;
    }

    /**
     * @return the dateModel3
     */
    public LineChartModel getDateModel3() {
        return dateModel3;
    }

    /**
     * @param dateModel3 the dateModel3 to set
     */
    public void setDateModel3(LineChartModel dateModel3) {
        this.dateModel3 = dateModel3;
    }

    /**
     * @return the dateModel5
     */
    public LineChartModel getDateModel5() {
        return dateModel5;
    }

    /**
     * @param dateModel5 the dateModel5 to set
     */
    public void setDateModel5(LineChartModel dateModel5) {
        this.dateModel5 = dateModel5;
    }

    /**
     * @return the dateModel6
     */
    public LineChartModel getDateModel6() {
        return dateModel6;
    }

    /**
     * @param dateModel6 the dateModel6 to set
     */
    public void setDateModel6(LineChartModel dateModel6) {
        this.dateModel6 = dateModel6;
    }

    /**
     * @return the dateModel9
     */
    public LineChartModel getDateModel9() {
        return dateModel9;
    }

    /**
     * @param dateModel9 the dateModel9 to set
     */
    public void setDateModel9(LineChartModel dateModel9) {
        this.dateModel9 = dateModel9;
    }

    /**
     * @return the dateModel10
     */
    public LineChartModel getDateModel10() {
        return dateModel10;
    }

    /**
     * @param dateModel10 the dateModel10 to set
     */
    public void setDateModel10(LineChartModel dateModel10) {
        this.dateModel10 = dateModel10;
    }

    /**
     * @return the dateModel11
     */
    public LineChartModel getDateModel11() {
        return dateModel11;
    }

    /**
     * @param dateModel11 the dateModel11 to set
     */
    public void setDateModel11(LineChartModel dateModel11) {
        this.dateModel11 = dateModel11;
    }

    /**
     * @return the dateModel15
     */
    public LineChartModel getDateModel15() {
        return dateModel15;
    }

    /**
     * @param dateModel15 the dateModel15 to set
     */
    public void setDateModel15(LineChartModel dateModel15) {
        this.dateModel15 = dateModel15;
    }

    /**
     * @return the dateModel23
     */
    public LineChartModel getDateModel23() {
        return dateModel23;
    }

    /**
     * @param dateModel23 the dateModel23 to set
     */
    public void setDateModel23(LineChartModel dateModel23) {
        this.dateModel23 = dateModel23;
    }

    private java.util.List<org.hl7.ws.meddev.Hl7MedicalDevices> findAll_4() {
        // Note that the injected javax.xml.ws.Service reference as well as port objects are not thread safe.
        // If the calling of port operations may lead to race condition some synchronization is required.
        org.hl7.ws.meddev.MedicaldeviceHL7 port = service_5.getMedicaldeviceHL7Port();
        return port.findAll();
    }

    /**
     * @return the medicalitems
     */
    public List<medicalinfo> getMedicalitems() {
        return medicalitems;
    }

    /**
     * @param medicalitems the medicalitems to set
     */
    public void setMedicalitems(List<medicalinfo> medicalitems) {
        this.medicalitems = medicalitems;
    }

    /**
     * @return the b1
     */
    public boolean isB1() {
        return b1;
    }

    /**
     * @param b1 the b1 to set
     */
    public void setB1(boolean b1) {
        this.b1 = b1;
    }

    /**
     * @return the b2
     */
    public boolean isB2() {
        return b2;
    }

    /**
     * @param b2 the b2 to set
     */
    public void setB2(boolean b2) {
        this.b2 = b2;
    }

    /**
     * @return the b3
     */
    public boolean isB3() {
        return b3;
    }

    /**
     * @param b3 the b3 to set
     */
    public void setB3(boolean b3) {
        this.b3 = b3;
    }

    /**
     * @return the b5
     */
    public boolean isB5() {
        return b5;
    }

    /**
     * @param b5 the b5 to set
     */
    public void setB5(boolean b5) {
        this.b5 = b5;
    }

    /**
     * @return the b6
     */
    public boolean isB6() {
        return b6;
    }

    /**
     * @param b6 the b6 to set
     */
    public void setB6(boolean b6) {
        this.b6 = b6;
    }

    /**
     * @return the b9
     */
    public boolean isB9() {
        return b9;
    }

    /**
     * @param b9 the b9 to set
     */
    public void setB9(boolean b9) {
        this.b9 = b9;
    }

    /**
     * @return the b10
     */
    public boolean isB10() {
        return b10;
    }

    /**
     * @param b10 the b10 to set
     */
    public void setB10(boolean b10) {
        this.b10 = b10;
    }

    /**
     * @return the b11
     */
    public boolean isB11() {
        return b11;
    }

    /**
     * @param b11 the b11 to set
     */
    public void setB11(boolean b11) {
        this.b11 = b11;
    }

    /**
     * @return the b15
     */
    public boolean isB15() {
        return b15;
    }

    /**
     * @param b15 the b15 to set
     */
    public void setB15(boolean b15) {
        this.b15 = b15;
    }

    /**
     * @return the b23
     */
    public boolean isB23() {
        return b23;
    }

    /**
     * @param b23 the b23 to set
     */
    public void setB23(boolean b23) {
        this.b23 = b23;
    }
}
