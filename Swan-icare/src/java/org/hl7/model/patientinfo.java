/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

/**
 *
 * @author tdim
 */
public class patientinfo {
    private Long id;
    private String name;
    private String status;
    private String devicestatus;
    private String currentstatus;
    private String medicaldevid;
    private String medicaldevstatus;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the devicestatus
     */
    public String getDevicestatus() {
        return devicestatus;
    }

    /**
     * @param devicestatus the devicestatus to set
     */
    public void setDevicestatus(String devicestatus) {
        this.devicestatus = devicestatus;
    }

    /**
     * @return the currentstatus
     */
    public String getCurrentstatus() {
        return currentstatus;
    }

    /**
     * @param currentstatus the currentstatus to set
     */
    public void setCurrentstatus(String currentstatus) {
        this.currentstatus = currentstatus;
    }

    /**
     * @return the medicaldevid
     */
    public String getMedicaldevid() {
        return medicaldevid;
    }

    /**
     * @param medicaldevid the medicaldevid to set
     */
    public void setMedicaldevid(String medicaldevid) {
        this.medicaldevid = medicaldevid;
    }

    /**
     * @return the medicaldevstatus
     */
    public String getMedicaldevstatus() {
        return medicaldevstatus;
    }

    /**
     * @param medicaldevstatus the medicaldevstatus to set
     */
    public void setMedicaldevstatus(String medicaldevstatus) {
        this.medicaldevstatus = medicaldevstatus;
    }
    
}
