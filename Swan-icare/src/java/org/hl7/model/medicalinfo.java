/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hl7.model;

/**
 *
 * @author Thomas Dimakopoulos
 */
public class medicalinfo {
    private String name;
    private String serial;
    private String status;
    private String battery;
    private String status1;
    private String status2;
    private String status3;
    private String status4;
    private String status5;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the serial
     */
    public String getSerial() {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial) {
        this.serial = serial;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the battery
     */
    public String getBattery() {
        return battery;
    }

    /**
     * @param battery the battery to set
     */
    public void setBattery(String battery) {
        this.battery = battery;
    }

    /**
     * @return the status1
     */
    public String getStatus1() {
        return status1;
    }

    /**
     * @param status1 the status1 to set
     */
    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    /**
     * @return the status2
     */
    public String getStatus2() {
        return status2;
    }

    /**
     * @param status2 the status2 to set
     */
    public void setStatus2(String status2) {
        this.status2 = status2;
    }

    /**
     * @return the status3
     */
    public String getStatus3() {
        return status3;
    }

    /**
     * @param status3 the status3 to set
     */
    public void setStatus3(String status3) {
        this.status3 = status3;
    }

    /**
     * @return the status4
     */
    public String getStatus4() {
        return status4;
    }

    /**
     * @param status4 the status4 to set
     */
    public void setStatus4(String status4) {
        this.status4 = status4;
    }

    /**
     * @return the status5
     */
    public String getStatus5() {
        return status5;
    }

    /**
     * @param status5 the status5 to set
     */
    public void setStatus5(String status5) {
        this.status5 = status5;
    }
    
}
