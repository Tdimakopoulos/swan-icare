
package org.hl7.ws.roles;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for edit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="edit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hL7Roles" type="{http://roles.ws.hl7.org/}hl7Roles" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edit", propOrder = {
    "hl7Roles"
})
public class Edit {

    @XmlElement(name = "hL7Roles")
    protected Hl7Roles hl7Roles;

    /**
     * Gets the value of the hl7Roles property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7Roles }
     *     
     */
    public Hl7Roles getHL7Roles() {
        return hl7Roles;
    }

    /**
     * Sets the value of the hl7Roles property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7Roles }
     *     
     */
    public void setHL7Roles(Hl7Roles value) {
        this.hl7Roles = value;
    }

}
