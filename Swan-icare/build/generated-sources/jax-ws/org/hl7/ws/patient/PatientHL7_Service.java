
package org.hl7.ws.patient;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-2b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "PatientHL7", targetNamespace = "http://patient.ws.hl7.org/", wsdlLocation = "http://localhost:8080/PatientHL7/PatientHL7?WSDL")
public class PatientHL7_Service
    extends Service
{

    private final static URL PATIENTHL7_WSDL_LOCATION;
    private final static WebServiceException PATIENTHL7_EXCEPTION;
    private final static QName PATIENTHL7_QNAME = new QName("http://patient.ws.hl7.org/", "PatientHL7");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/PatientHL7/PatientHL7?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        PATIENTHL7_WSDL_LOCATION = url;
        PATIENTHL7_EXCEPTION = e;
    }

    public PatientHL7_Service() {
        super(__getWsdlLocation(), PATIENTHL7_QNAME);
    }

    public PatientHL7_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), PATIENTHL7_QNAME, features);
    }

    public PatientHL7_Service(URL wsdlLocation) {
        super(wsdlLocation, PATIENTHL7_QNAME);
    }

    public PatientHL7_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, PATIENTHL7_QNAME, features);
    }

    public PatientHL7_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public PatientHL7_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns PatientHL7
     */
    @WebEndpoint(name = "PatientHL7Port")
    public PatientHL7 getPatientHL7Port() {
        return super.getPort(new QName("http://patient.ws.hl7.org/", "PatientHL7Port"), PatientHL7.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns PatientHL7
     */
    @WebEndpoint(name = "PatientHL7Port")
    public PatientHL7 getPatientHL7Port(WebServiceFeature... features) {
        return super.getPort(new QName("http://patient.ws.hl7.org/", "PatientHL7Port"), PatientHL7.class, features);
    }

    private static URL __getWsdlLocation() {
        if (PATIENTHL7_EXCEPTION!= null) {
            throw PATIENTHL7_EXCEPTION;
        }
        return PATIENTHL7_WSDL_LOCATION;
    }

}
