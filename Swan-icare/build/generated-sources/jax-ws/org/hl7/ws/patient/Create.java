
package org.hl7.ws.patient;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for create complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="create">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hL7Patient" type="{http://patient.ws.hl7.org/}hl7Patient" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "create", propOrder = {
    "hl7Patient"
})
public class Create {

    @XmlElement(name = "hL7Patient")
    protected Hl7Patient hl7Patient;

    /**
     * Gets the value of the hl7Patient property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7Patient }
     *     
     */
    public Hl7Patient getHL7Patient() {
        return hl7Patient;
    }

    /**
     * Sets the value of the hl7Patient property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7Patient }
     *     
     */
    public void setHL7Patient(Hl7Patient value) {
        this.hl7Patient = value;
    }

}
