
package org.hl7.ws.assoc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for remove complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="remove">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hL7Associations" type="{http://assoc.ws.hl7.org/}hl7Associations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "remove", propOrder = {
    "hl7Associations"
})
public class Remove {

    @XmlElement(name = "hL7Associations")
    protected Hl7Associations hl7Associations;

    /**
     * Gets the value of the hl7Associations property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7Associations }
     *     
     */
    public Hl7Associations getHL7Associations() {
        return hl7Associations;
    }

    /**
     * Sets the value of the hl7Associations property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7Associations }
     *     
     */
    public void setHL7Associations(Hl7Associations value) {
        this.hl7Associations = value;
    }

}
