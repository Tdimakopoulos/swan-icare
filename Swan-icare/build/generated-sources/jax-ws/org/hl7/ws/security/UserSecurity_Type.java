
package org.hl7.ws.security;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for userSecurity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="userSecurity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currentstatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="iperson" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="irole" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="lastlogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "userSecurity", propOrder = {
    "currentstatus",
    "id",
    "iperson",
    "irole",
    "lastlogin",
    "password",
    "username"
})
public class UserSecurity_Type {

    protected String currentstatus;
    protected Long id;
    protected Long iperson;
    protected Long irole;
    protected String lastlogin;
    protected String password;
    protected String username;

    /**
     * Gets the value of the currentstatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentstatus() {
        return currentstatus;
    }

    /**
     * Sets the value of the currentstatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentstatus(String value) {
        this.currentstatus = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the iperson property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIperson() {
        return iperson;
    }

    /**
     * Sets the value of the iperson property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIperson(Long value) {
        this.iperson = value;
    }

    /**
     * Gets the value of the irole property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIrole() {
        return irole;
    }

    /**
     * Sets the value of the irole property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIrole(Long value) {
        this.irole = value;
    }

    /**
     * Gets the value of the lastlogin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastlogin() {
        return lastlogin;
    }

    /**
     * Sets the value of the lastlogin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastlogin(String value) {
        this.lastlogin = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the username property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the value of the username property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUsername(String value) {
        this.username = value;
    }

}
