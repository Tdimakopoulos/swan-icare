
package org.hl7.ws.meddev;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for remove complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="remove">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hL7MedicalDevices" type="{http://meddev.ws.hl7.org/}hl7MedicalDevices" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "remove", propOrder = {
    "hl7MedicalDevices"
})
public class Remove {

    @XmlElement(name = "hL7MedicalDevices")
    protected Hl7MedicalDevices hl7MedicalDevices;

    /**
     * Gets the value of the hl7MedicalDevices property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7MedicalDevices }
     *     
     */
    public Hl7MedicalDevices getHL7MedicalDevices() {
        return hl7MedicalDevices;
    }

    /**
     * Sets the value of the hl7MedicalDevices property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7MedicalDevices }
     *     
     */
    public void setHL7MedicalDevices(Hl7MedicalDevices value) {
        this.hl7MedicalDevices = value;
    }

}
