
package org.hl7.ws.meddev;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7MedicalDevices complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7MedicalDevices">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="info1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="networkip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="networkport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ppnetworkip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ppnetworkport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serial" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7MedicalDevices", propOrder = {
    "id",
    "info1",
    "info2",
    "info3",
    "info4",
    "info5",
    "info6",
    "name",
    "networkip",
    "networkport",
    "ppnetworkip",
    "ppnetworkport",
    "serial",
    "type"
})
public class Hl7MedicalDevices {

    protected Long id;
    protected String info1;
    protected String info2;
    protected String info3;
    protected String info4;
    protected String info5;
    protected String info6;
    protected String name;
    protected String networkip;
    protected String networkport;
    protected String ppnetworkip;
    protected String ppnetworkport;
    protected String serial;
    protected String type;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the info1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * Sets the value of the info1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo1(String value) {
        this.info1 = value;
    }

    /**
     * Gets the value of the info2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * Sets the value of the info2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo2(String value) {
        this.info2 = value;
    }

    /**
     * Gets the value of the info3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * Sets the value of the info3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo3(String value) {
        this.info3 = value;
    }

    /**
     * Gets the value of the info4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * Sets the value of the info4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo4(String value) {
        this.info4 = value;
    }

    /**
     * Gets the value of the info5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo5() {
        return info5;
    }

    /**
     * Sets the value of the info5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo5(String value) {
        this.info5 = value;
    }

    /**
     * Gets the value of the info6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo6() {
        return info6;
    }

    /**
     * Sets the value of the info6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo6(String value) {
        this.info6 = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the networkip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkip() {
        return networkip;
    }

    /**
     * Sets the value of the networkip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkip(String value) {
        this.networkip = value;
    }

    /**
     * Gets the value of the networkport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNetworkport() {
        return networkport;
    }

    /**
     * Sets the value of the networkport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNetworkport(String value) {
        this.networkport = value;
    }

    /**
     * Gets the value of the ppnetworkip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPpnetworkip() {
        return ppnetworkip;
    }

    /**
     * Sets the value of the ppnetworkip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPpnetworkip(String value) {
        this.ppnetworkip = value;
    }

    /**
     * Gets the value of the ppnetworkport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPpnetworkport() {
        return ppnetworkport;
    }

    /**
     * Sets the value of the ppnetworkport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPpnetworkport(String value) {
        this.ppnetworkport = value;
    }

    /**
     * Gets the value of the serial property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSerial() {
        return serial;
    }

    /**
     * Sets the value of the serial property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSerial(String value) {
        this.serial = value;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
