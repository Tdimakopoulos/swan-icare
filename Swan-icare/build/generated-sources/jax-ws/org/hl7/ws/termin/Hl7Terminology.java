
package org.hl7.ws.termin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7Terminology complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7Terminology">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hl7area" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hl7key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hl7value" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="textfield1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textfield2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textfield3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textfield4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7Terminology", propOrder = {
    "hl7Area",
    "hl7Key",
    "hl7Value",
    "id",
    "textfield1",
    "textfield2",
    "textfield3",
    "textfield4"
})
public class Hl7Terminology {

    @XmlElement(name = "hl7area")
    protected String hl7Area;
    @XmlElement(name = "hl7key")
    protected String hl7Key;
    @XmlElement(name = "hl7value")
    protected String hl7Value;
    protected Long id;
    protected String textfield1;
    protected String textfield2;
    protected String textfield3;
    protected String textfield4;

    /**
     * Gets the value of the hl7Area property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHl7Area() {
        return hl7Area;
    }

    /**
     * Sets the value of the hl7Area property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHl7Area(String value) {
        this.hl7Area = value;
    }

    /**
     * Gets the value of the hl7Key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHl7Key() {
        return hl7Key;
    }

    /**
     * Sets the value of the hl7Key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHl7Key(String value) {
        this.hl7Key = value;
    }

    /**
     * Gets the value of the hl7Value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHl7Value() {
        return hl7Value;
    }

    /**
     * Sets the value of the hl7Value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHl7Value(String value) {
        this.hl7Value = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the textfield1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextfield1() {
        return textfield1;
    }

    /**
     * Sets the value of the textfield1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextfield1(String value) {
        this.textfield1 = value;
    }

    /**
     * Gets the value of the textfield2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextfield2() {
        return textfield2;
    }

    /**
     * Sets the value of the textfield2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextfield2(String value) {
        this.textfield2 = value;
    }

    /**
     * Gets the value of the textfield3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextfield3() {
        return textfield3;
    }

    /**
     * Sets the value of the textfield3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextfield3(String value) {
        this.textfield3 = value;
    }

    /**
     * Gets the value of the textfield4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTextfield4() {
        return textfield4;
    }

    /**
     * Sets the value of the textfield4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTextfield4(String value) {
        this.textfield4 = value;
    }

}
