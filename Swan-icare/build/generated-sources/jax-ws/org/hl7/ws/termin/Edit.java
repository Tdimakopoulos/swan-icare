
package org.hl7.ws.termin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for edit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="edit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hL7Terminology" type="{http://termin.ws.hl7.org/}hl7Terminology" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edit", propOrder = {
    "hl7Terminology"
})
public class Edit {

    @XmlElement(name = "hL7Terminology")
    protected Hl7Terminology hl7Terminology;

    /**
     * Gets the value of the hl7Terminology property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7Terminology }
     *     
     */
    public Hl7Terminology getHL7Terminology() {
        return hl7Terminology;
    }

    /**
     * Sets the value of the hl7Terminology property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7Terminology }
     *     
     */
    public void setHL7Terminology(Hl7Terminology value) {
        this.hl7Terminology = value;
    }

}
