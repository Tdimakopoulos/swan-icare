
package org.hl7.ws.roles;

import java.util.List;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-2b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "RoleHL7", targetNamespace = "http://roles.ws.hl7.org/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface RoleHL7 {


    /**
     * 
     * @return
     *     returns int
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "count", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.Count")
    @ResponseWrapper(localName = "countResponse", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.CountResponse")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/countRequest", output = "http://roles.ws.hl7.org/RoleHL7/countResponse")
    public int count();

    /**
     * 
     * @param id
     * @return
     *     returns org.hl7.ws.roles.Hl7Roles
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "find", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.Find")
    @ResponseWrapper(localName = "findResponse", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.FindResponse")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/findRequest", output = "http://roles.ws.hl7.org/RoleHL7/findResponse")
    public Hl7Roles find(
        @WebParam(name = "id", targetNamespace = "")
        Object id);

    /**
     * 
     * @param hL7Roles
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "remove", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.Remove")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/remove")
    public void remove(
        @WebParam(name = "hL7Roles", targetNamespace = "")
        Hl7Roles hL7Roles);

    /**
     * 
     * @param hL7Roles
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "create", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.Create")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/create")
    public void create(
        @WebParam(name = "hL7Roles", targetNamespace = "")
        Hl7Roles hL7Roles);

    /**
     * 
     * @return
     *     returns java.util.List<org.hl7.ws.roles.Hl7Roles>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAll", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.FindAll")
    @ResponseWrapper(localName = "findAllResponse", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.FindAllResponse")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/findAllRequest", output = "http://roles.ws.hl7.org/RoleHL7/findAllResponse")
    public List<Hl7Roles> findAll();

    /**
     * 
     * @param hL7Roles
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "edit", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.Edit")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/edit")
    public void edit(
        @WebParam(name = "hL7Roles", targetNamespace = "")
        Hl7Roles hL7Roles);

    /**
     * 
     * @param range
     * @return
     *     returns java.util.List<org.hl7.ws.roles.Hl7Roles>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findRange", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.FindRange")
    @ResponseWrapper(localName = "findRangeResponse", targetNamespace = "http://roles.ws.hl7.org/", className = "org.hl7.ws.roles.FindRangeResponse")
    @Action(input = "http://roles.ws.hl7.org/RoleHL7/findRangeRequest", output = "http://roles.ws.hl7.org/RoleHL7/findRangeResponse")
    public List<Hl7Roles> findRange(
        @WebParam(name = "range", targetNamespace = "")
        List<Integer> range);

}
