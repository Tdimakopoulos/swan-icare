
package org.hl7.ws.roles;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-2b01 
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "RoleHL7", targetNamespace = "http://roles.ws.hl7.org/", wsdlLocation = "http://localhost:8080/RoleHL7/RoleHL7?WSDL")
public class RoleHL7_Service
    extends Service
{

    private final static URL ROLEHL7_WSDL_LOCATION;
    private final static WebServiceException ROLEHL7_EXCEPTION;
    private final static QName ROLEHL7_QNAME = new QName("http://roles.ws.hl7.org/", "RoleHL7");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:8080/RoleHL7/RoleHL7?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        ROLEHL7_WSDL_LOCATION = url;
        ROLEHL7_EXCEPTION = e;
    }

    public RoleHL7_Service() {
        super(__getWsdlLocation(), ROLEHL7_QNAME);
    }

    public RoleHL7_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), ROLEHL7_QNAME, features);
    }

    public RoleHL7_Service(URL wsdlLocation) {
        super(wsdlLocation, ROLEHL7_QNAME);
    }

    public RoleHL7_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, ROLEHL7_QNAME, features);
    }

    public RoleHL7_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RoleHL7_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns RoleHL7
     */
    @WebEndpoint(name = "RoleHL7Port")
    public RoleHL7 getRoleHL7Port() {
        return super.getPort(new QName("http://roles.ws.hl7.org/", "RoleHL7Port"), RoleHL7.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns RoleHL7
     */
    @WebEndpoint(name = "RoleHL7Port")
    public RoleHL7 getRoleHL7Port(WebServiceFeature... features) {
        return super.getPort(new QName("http://roles.ws.hl7.org/", "RoleHL7Port"), RoleHL7.class, features);
    }

    private static URL __getWsdlLocation() {
        if (ROLEHL7_EXCEPTION!= null) {
            throw ROLEHL7_EXCEPTION;
        }
        return ROLEHL7_WSDL_LOCATION;
    }

}
