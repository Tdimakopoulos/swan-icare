
package org.hl7.ws.roles;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7Roles complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7Roles">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="rolearea" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rolename" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="roletype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7Roles", propOrder = {
    "id",
    "rolearea",
    "rolename",
    "roletype"
})
public class Hl7Roles {

    protected Long id;
    protected String rolearea;
    protected String rolename;
    protected String roletype;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the rolearea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolearea() {
        return rolearea;
    }

    /**
     * Sets the value of the rolearea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolearea(String value) {
        this.rolearea = value;
    }

    /**
     * Gets the value of the rolename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRolename() {
        return rolename;
    }

    /**
     * Sets the value of the rolename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRolename(String value) {
        this.rolename = value;
    }

    /**
     * Gets the value of the roletype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRoletype() {
        return roletype;
    }

    /**
     * Sets the value of the roletype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRoletype(String value) {
        this.roletype = value;
    }

}
