
package org.hl7.ws.obs;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7Observation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7Observation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dbldate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="info1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="info4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intvalue" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="lngdate" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="longvalue" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="lpatient" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stdname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strdate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="strvalue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7Observation", propOrder = {
    "dbldate",
    "id",
    "info1",
    "info2",
    "info3",
    "info4",
    "intvalue",
    "lngdate",
    "longvalue",
    "lpatient",
    "name",
    "stdname",
    "strdate",
    "strvalue"
})
public class Hl7Observation {

    protected double dbldate;
    protected Long id;
    protected String info1;
    protected String info2;
    protected String info3;
    protected String info4;
    protected int intvalue;
    protected Long lngdate;
    protected Long longvalue;
    protected Long lpatient;
    protected String name;
    protected String stdname;
    protected String strdate;
    protected String strvalue;

    /**
     * Gets the value of the dbldate property.
     * 
     */
    public double getDbldate() {
        return dbldate;
    }

    /**
     * Sets the value of the dbldate property.
     * 
     */
    public void setDbldate(double value) {
        this.dbldate = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the info1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo1() {
        return info1;
    }

    /**
     * Sets the value of the info1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo1(String value) {
        this.info1 = value;
    }

    /**
     * Gets the value of the info2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo2() {
        return info2;
    }

    /**
     * Sets the value of the info2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo2(String value) {
        this.info2 = value;
    }

    /**
     * Gets the value of the info3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo3() {
        return info3;
    }

    /**
     * Sets the value of the info3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo3(String value) {
        this.info3 = value;
    }

    /**
     * Gets the value of the info4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInfo4() {
        return info4;
    }

    /**
     * Sets the value of the info4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInfo4(String value) {
        this.info4 = value;
    }

    /**
     * Gets the value of the intvalue property.
     * 
     */
    public int getIntvalue() {
        return intvalue;
    }

    /**
     * Sets the value of the intvalue property.
     * 
     */
    public void setIntvalue(int value) {
        this.intvalue = value;
    }

    /**
     * Gets the value of the lngdate property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLngdate() {
        return lngdate;
    }

    /**
     * Sets the value of the lngdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLngdate(Long value) {
        this.lngdate = value;
    }

    /**
     * Gets the value of the longvalue property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLongvalue() {
        return longvalue;
    }

    /**
     * Sets the value of the longvalue property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLongvalue(Long value) {
        this.longvalue = value;
    }

    /**
     * Gets the value of the lpatient property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLpatient() {
        return lpatient;
    }

    /**
     * Sets the value of the lpatient property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLpatient(Long value) {
        this.lpatient = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the stdname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStdname() {
        return stdname;
    }

    /**
     * Sets the value of the stdname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStdname(String value) {
        this.stdname = value;
    }

    /**
     * Gets the value of the strdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrdate() {
        return strdate;
    }

    /**
     * Sets the value of the strdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrdate(String value) {
        this.strdate = value;
    }

    /**
     * Gets the value of the strvalue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrvalue() {
        return strvalue;
    }

    /**
     * Sets the value of the strvalue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrvalue(String value) {
        this.strvalue = value;
    }

}
