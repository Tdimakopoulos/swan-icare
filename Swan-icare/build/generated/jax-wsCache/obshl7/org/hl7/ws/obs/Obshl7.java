
package org.hl7.ws.obs;

import java.util.List;
import javax.jws.Oneway;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-2b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "obshl7", targetNamespace = "http://obs.ws.hl7.org/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface Obshl7 {


    /**
     * 
     * @return
     *     returns int
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "count", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.Count")
    @ResponseWrapper(localName = "countResponse", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.CountResponse")
    @Action(input = "http://obs.ws.hl7.org/obshl7/countRequest", output = "http://obs.ws.hl7.org/obshl7/countResponse")
    public int count();

    /**
     * 
     * @param id
     * @return
     *     returns org.hl7.ws.obs.Hl7Observation
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "find", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.Find")
    @ResponseWrapper(localName = "findResponse", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.FindResponse")
    @Action(input = "http://obs.ws.hl7.org/obshl7/findRequest", output = "http://obs.ws.hl7.org/obshl7/findResponse")
    public Hl7Observation find(
        @WebParam(name = "id", targetNamespace = "")
        Object id);

    /**
     * 
     * @param hL7Observation
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "remove", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.Remove")
    @Action(input = "http://obs.ws.hl7.org/obshl7/remove")
    public void remove(
        @WebParam(name = "hL7Observation", targetNamespace = "")
        Hl7Observation hL7Observation);

    /**
     * 
     * @param hL7Observation
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "create", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.Create")
    @Action(input = "http://obs.ws.hl7.org/obshl7/create")
    public void create(
        @WebParam(name = "hL7Observation", targetNamespace = "")
        Hl7Observation hL7Observation);

    /**
     * 
     * @return
     *     returns java.util.List<org.hl7.ws.obs.Hl7Observation>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAll", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.FindAll")
    @ResponseWrapper(localName = "findAllResponse", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.FindAllResponse")
    @Action(input = "http://obs.ws.hl7.org/obshl7/findAllRequest", output = "http://obs.ws.hl7.org/obshl7/findAllResponse")
    public List<Hl7Observation> findAll();

    /**
     * 
     * @param hL7Observation
     */
    @WebMethod
    @Oneway
    @RequestWrapper(localName = "edit", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.Edit")
    @Action(input = "http://obs.ws.hl7.org/obshl7/edit")
    public void edit(
        @WebParam(name = "hL7Observation", targetNamespace = "")
        Hl7Observation hL7Observation);

    /**
     * 
     * @param range
     * @return
     *     returns java.util.List<org.hl7.ws.obs.Hl7Observation>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findRange", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.FindRange")
    @ResponseWrapper(localName = "findRangeResponse", targetNamespace = "http://obs.ws.hl7.org/", className = "org.hl7.ws.obs.FindRangeResponse")
    @Action(input = "http://obs.ws.hl7.org/obshl7/findRangeRequest", output = "http://obs.ws.hl7.org/obshl7/findRangeResponse")
    public List<Hl7Observation> findRange(
        @WebParam(name = "range", targetNamespace = "")
        List<Integer> range);

}
