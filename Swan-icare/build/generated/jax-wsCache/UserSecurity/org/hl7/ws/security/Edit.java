
package org.hl7.ws.security;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for edit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="edit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="userSecurity" type="{http://security.ws.hl7.org/}userSecurity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "edit", propOrder = {
    "userSecurity"
})
public class Edit {

    protected UserSecurity_Type userSecurity;

    /**
     * Gets the value of the userSecurity property.
     * 
     * @return
     *     possible object is
     *     {@link UserSecurity_Type }
     *     
     */
    public UserSecurity_Type getUserSecurity() {
        return userSecurity;
    }

    /**
     * Sets the value of the userSecurity property.
     * 
     * @param value
     *     allowed object is
     *     {@link UserSecurity_Type }
     *     
     */
    public void setUserSecurity(UserSecurity_Type value) {
        this.userSecurity = value;
    }

}
