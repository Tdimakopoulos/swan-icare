
package org.hl7.ws.doctor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for create complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="create">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hL7Doctor" type="{http://doctor.ws.hl7.org/}hl7Doctor" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "create", propOrder = {
    "hl7Doctor"
})
public class Create {

    @XmlElement(name = "hL7Doctor")
    protected Hl7Doctor hl7Doctor;

    /**
     * Gets the value of the hl7Doctor property.
     * 
     * @return
     *     possible object is
     *     {@link Hl7Doctor }
     *     
     */
    public Hl7Doctor getHL7Doctor() {
        return hl7Doctor;
    }

    /**
     * Sets the value of the hl7Doctor property.
     * 
     * @param value
     *     allowed object is
     *     {@link Hl7Doctor }
     *     
     */
    public void setHL7Doctor(Hl7Doctor value) {
        this.hl7Doctor = value;
    }

}
