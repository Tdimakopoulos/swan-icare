
package org.hl7.ws.doctor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for hl7Doctor complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="hl7Doctor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="address1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="department" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="mobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo10" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo11" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo12" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo13" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo5" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo6" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo7" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo8" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="workinfo9" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "hl7Doctor", propOrder = {
    "address1",
    "address2",
    "address3",
    "address4",
    "address5",
    "address6",
    "address7",
    "department",
    "email",
    "id",
    "mobile",
    "name",
    "phone",
    "surname",
    "workinfo1",
    "workinfo10",
    "workinfo11",
    "workinfo12",
    "workinfo13",
    "workinfo2",
    "workinfo3",
    "workinfo4",
    "workinfo5",
    "workinfo6",
    "workinfo7",
    "workinfo8",
    "workinfo9"
})
public class Hl7Doctor {

    protected String address1;
    protected String address2;
    protected String address3;
    protected String address4;
    protected String address5;
    protected String address6;
    protected String address7;
    protected String department;
    protected String email;
    protected Long id;
    protected String mobile;
    protected String name;
    protected String phone;
    protected String surname;
    protected String workinfo1;
    protected String workinfo10;
    protected String workinfo11;
    protected String workinfo12;
    protected String workinfo13;
    protected String workinfo2;
    protected String workinfo3;
    protected String workinfo4;
    protected String workinfo5;
    protected String workinfo6;
    protected String workinfo7;
    protected String workinfo8;
    protected String workinfo9;

    /**
     * Gets the value of the address1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the value of the address1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress1(String value) {
        this.address1 = value;
    }

    /**
     * Gets the value of the address2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the value of the address2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress2(String value) {
        this.address2 = value;
    }

    /**
     * Gets the value of the address3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress3() {
        return address3;
    }

    /**
     * Sets the value of the address3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress3(String value) {
        this.address3 = value;
    }

    /**
     * Gets the value of the address4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress4() {
        return address4;
    }

    /**
     * Sets the value of the address4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress4(String value) {
        this.address4 = value;
    }

    /**
     * Gets the value of the address5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress5() {
        return address5;
    }

    /**
     * Sets the value of the address5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress5(String value) {
        this.address5 = value;
    }

    /**
     * Gets the value of the address6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress6() {
        return address6;
    }

    /**
     * Sets the value of the address6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress6(String value) {
        this.address6 = value;
    }

    /**
     * Gets the value of the address7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddress7() {
        return address7;
    }

    /**
     * Sets the value of the address7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddress7(String value) {
        this.address7 = value;
    }

    /**
     * Gets the value of the department property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDepartment() {
        return department;
    }

    /**
     * Sets the value of the department property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDepartment(String value) {
        this.department = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the mobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the value of the mobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobile(String value) {
        this.mobile = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the phone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets the value of the phone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the workinfo1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo1() {
        return workinfo1;
    }

    /**
     * Sets the value of the workinfo1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo1(String value) {
        this.workinfo1 = value;
    }

    /**
     * Gets the value of the workinfo10 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo10() {
        return workinfo10;
    }

    /**
     * Sets the value of the workinfo10 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo10(String value) {
        this.workinfo10 = value;
    }

    /**
     * Gets the value of the workinfo11 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo11() {
        return workinfo11;
    }

    /**
     * Sets the value of the workinfo11 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo11(String value) {
        this.workinfo11 = value;
    }

    /**
     * Gets the value of the workinfo12 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo12() {
        return workinfo12;
    }

    /**
     * Sets the value of the workinfo12 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo12(String value) {
        this.workinfo12 = value;
    }

    /**
     * Gets the value of the workinfo13 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo13() {
        return workinfo13;
    }

    /**
     * Sets the value of the workinfo13 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo13(String value) {
        this.workinfo13 = value;
    }

    /**
     * Gets the value of the workinfo2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo2() {
        return workinfo2;
    }

    /**
     * Sets the value of the workinfo2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo2(String value) {
        this.workinfo2 = value;
    }

    /**
     * Gets the value of the workinfo3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo3() {
        return workinfo3;
    }

    /**
     * Sets the value of the workinfo3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo3(String value) {
        this.workinfo3 = value;
    }

    /**
     * Gets the value of the workinfo4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo4() {
        return workinfo4;
    }

    /**
     * Sets the value of the workinfo4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo4(String value) {
        this.workinfo4 = value;
    }

    /**
     * Gets the value of the workinfo5 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo5() {
        return workinfo5;
    }

    /**
     * Sets the value of the workinfo5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo5(String value) {
        this.workinfo5 = value;
    }

    /**
     * Gets the value of the workinfo6 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo6() {
        return workinfo6;
    }

    /**
     * Sets the value of the workinfo6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo6(String value) {
        this.workinfo6 = value;
    }

    /**
     * Gets the value of the workinfo7 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo7() {
        return workinfo7;
    }

    /**
     * Sets the value of the workinfo7 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo7(String value) {
        this.workinfo7 = value;
    }

    /**
     * Gets the value of the workinfo8 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo8() {
        return workinfo8;
    }

    /**
     * Sets the value of the workinfo8 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo8(String value) {
        this.workinfo8 = value;
    }

    /**
     * Gets the value of the workinfo9 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorkinfo9() {
        return workinfo9;
    }

    /**
     * Sets the value of the workinfo9 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorkinfo9(String value) {
        this.workinfo9 = value;
    }

}
